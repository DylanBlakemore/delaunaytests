from scipy.spatial import Delaunay
import numpy as np
import vmptclib

def count(P):
    tri = Delaunay(P)
    N_t = P.shape[0]
    k_D = 0
    
    indptr = tri.vertex_neighbor_vertices[1]
    indices = tri.vertex_neighbor_vertices[0]
        
    for i in xrange( N_t ):
        k_D = k_D + len(indptr[indices[i]:indices[i+1]])
        
    return k_D/float(N_t)

def loadlm(fname, N_f, lpf, t_0):
    [start_byte, cur_time] = vmptclib.getStartingPoint(fname, t_0);
    cur_byte = start_byte
    [nlines_loaded,cur_byte,cur_time,lines_arr] = vmptclib.getFrameBatch(fname, cur_byte, cur_time, lpf, N_f, 0)
    
    lines = np.reshape(lines_arr,(-1,7))
     
    return lines

