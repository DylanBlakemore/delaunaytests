import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from lib import deltests
from lib import frame

if __name__ == "__main__":
    #fname = "/home/dylan/MastersLMData/Raw/09_09_2014/Runs/vmpt_001_02.1.16_1.lm"
    #fname = "/home/dylan/MastersLMData/Raw/09_09_2014/Runs/vmpt_009_04.2.16_1.lm"
    #fname = "/home/dylan/MastersLMData/Raw/09_09_2014/Runs/vmpt_013_06.1.16_1.lm"
    #fname = "/home/dylan/MastersLMData/Raw/09_09_2014/Runs/vmpt_020_08.1.16_1.lm"
    fname = "/home/dylan/MastersLMData/Raw/09_09_2014/Runs/vmpt_028_12.1.16_1.lm"
    N_f = 50
    N_t = 8
    lpt = 50
    lpf = lpt*N_t
    t_0 = 0
    
    k_D_arr = np.zeros((N_f,1))
    N_p_arr = np.zeros((N_f,1))
    
    lines = deltests.loadlm(fname, N_f, lpf, t_0)
    
    split_indices = range(lpf, N_f*lpf, lpf)
    split_data = np.split(lines, split_indices)
    
    for i in range(0, N_f):
        frame_i = frame.Frame(split_data[i])
        
        P = frame_i.getAllPoints(5.0)
        
        N_p_arr[i] = P.shape[0]
        k_D_arr[i] = deltests.count(P)
        
    k_D_avg = k_D_arr.mean()
    k_D_std = k_D_arr.std()
    print("kD = " + str(k_D_avg) + " (" + str(k_D_std) + ")")
    
    N_p_avg = N_p_arr.mean()
    N_p_std = N_p_arr.std()
    
    print("Np = " + str(N_p_avg) + " (" + str(N_p_std) + ")")
    
