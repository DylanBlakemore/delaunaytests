import numpy as np
from lib import deltests
from lib import frame
from lib import lor

if __name__ == "__main__":
    N_p = 100000   # Number of data points
    d   = 3     # Dimensions
    N_i = 500   # Number of iterations
    
    k_D_arr = np.zeros((N_i,1)) # Array of neighbour averages
    
    for i in xrange(0, N_i):
        P = np.random.rand(N_p, d)
        k_D_arr[i] = deltests.count(P)
    
    avg = k_D_arr.mean()
    std = k_D_arr.std()
    print(str(avg) + " (" + str(std) + ")")
    
